﻿using Common;
using Entity.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Model.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fsb.smartcontractapi.Attribute
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IConfiguration _configuration;

        public JwtMiddleware(RequestDelegate next, IConfiguration configuration)
        {
            _next = next;
            _configuration = configuration;
        }

        public async Task Invoke(HttpContext context, FsbSmartContractContext dbContext)
        {
            //await _next(context);
            var path = context.Request.Path.Value;
            if (path.ToLower().Contains("/account/authencate"))
            {
                await _next(context);
            }
            else
            {
                var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
                if (string.IsNullOrEmpty(token))
                {
                    context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                    Response<string> res = new Response<string>();
                    res.Code = StatusCodes.Status401Unauthorized;
                    res.Message = "Token invalid!";
                    await context.Response.WriteAsync(JsonConvert.SerializeObject(res));
                }
                else
                {
                    try
                    {
                        var claims = TokenUtil.ValidateToken(token, _configuration["Tokens:Key"], _configuration["Tokens:Issuer"]);
                        context.User = claims; //Xóa dòng này đi là lỗi
                        //var user = claims.FindFirst(x => x.Type == ClaimTypes.NameIdentifier).Value;
                        //var roles = claims.FindFirst(x => x.Type == ClaimTypes.Role).Value;
                        await _next(context);
                    }
                    catch (Exception ex)
                    {
                        Response<string> res = new Response<string>();
                        res.Code = StatusCodes.Status401Unauthorized;
                        res.Message = "Token invalid!";
                        //res.DataError = ex.ToString();
                        context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                        await context.Response.WriteAsync(JsonConvert.SerializeObject(res));
                    }
                }
            }
        }
    }
}
