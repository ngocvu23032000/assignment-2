﻿using Model.Requests;
using Model.Response;
using Model.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fsb.smartcontract.Services.Role
{
    public interface IRoleService
    {
        TableResponse<RoleViewModel> GetListRole(SearchRoleModel search);
        Response<string> CreateRole(RoleModel model);
        Response<string> DeleteRole(RoleModel model);
        Response<RoleModel> GetRoleById(RoleModel model);
        Response<string> UpdateRole(RoleModel model);
    }
}
