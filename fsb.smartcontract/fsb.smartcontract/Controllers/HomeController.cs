﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using fsb.smartcontract.Models;
using NToastNotify;
using Microsoft.AspNetCore.Http;
using fsb.smartcontract.Services.User;
using System.Net.Http;

namespace fsb.smartcontract.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IToastNotification _toastNotification;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUserService _userService;

        public HomeController(
            ILogger<HomeController> logger,
            IHttpClientFactory factory,
            IToastNotification toastNotification,
            IHttpContextAccessor httpContextAccessor,
            IUserService userService
            ) : base(factory)
        {
            _logger = logger;
            _toastNotification = toastNotification;
            _httpContextAccessor = httpContextAccessor;
            _userService = userService;
        }

        [Route("bang-dieu-khien")]
        public IActionResult Dashboard()
        {
            //ViewBag.TotalUser = _userService.GetAllUser().Count();
            return View();
        }
    }
}
