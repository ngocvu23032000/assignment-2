﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Role
{
    public class RoleModel : ModelBase
    {
        public Guid Id { get; set; }
        public string RoleName { get; set; }
    }
}
