﻿using Model.Response;
using Model.UserInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fsb.smartcontract.Services.User
{
    public interface IUserService
    {
        Response<UserViewModel> GetUserByUserName(UserViewModel model);
    }
}
